/// SPDX-License-Identifier: AGPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net"

	"os"
	"os/signal"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"leastfixedpoint.com/nscdn/pkg/hashtree"

	_ "github.com/mattn/go-sqlite3"
	"github.com/miekg/dns"
)

//go:generate ./metadata.sh

var (
	db   *sql.DB
	root = flag.String("root", "demo.nscdn.org", "Domain name root to serve")
	port = flag.Int("port", 53, "DNS port (both UDP and TCP) to listen on")
)

type NXDOMAIN struct{}

func (_ NXDOMAIN) Error() string {
	return "No such entry"
}

func recordsForName(queriedName, name string, reply *dns.Msg, includeTxt, isUdp bool) error {
	var pointerBlob []byte
	var p hashtree.Pointer

	row := db.QueryRow("select pointer from files where name = ?", name)
	err := row.Scan(&pointerBlob)
	switch err {
	case nil:
		err = p.Unmarshal(pointerBlob)
		if err != nil {
			return err
		}
		pName := p.Label() + "." + *root
		reply.Answer = append(reply.Answer, &dns.CNAME{
			Hdr:    dns.RR_Header{Name: queriedName, Rrtype: dns.TypeCNAME, Class: dns.ClassINET, Ttl: 60},
			Target: pName,
		})
		queriedName = pName
	case sql.ErrNoRows:
		p, err = hashtree.FromLabel(name)
		if err != nil {
			// label parsing error isn't fatal, it just means we don't
			// have such a record set
			return NXDOMAIN{}
		}
	default:
		return err
	}

	if !includeTxt {
		return nil
	}

	row = db.QueryRow("select body from chunks where hash = ?", p.Hash[:])
	var bodyBlob []byte
	err = row.Scan(&bodyBlob)
	switch err {
	case nil:
		const chunksize = 255
		txts := make([]string, 0, (len(bodyBlob)+chunksize-1)/chunksize)
		if isUdp && cap(txts) > 1 {
			reply.Truncated = true
		} else {
			// log.Infof("body for %x, length %d: %x", p.Hash[:], len(bodyBlob), bodyBlob)
			for len(bodyBlob) > 0 {
				s := len(bodyBlob)
				if s > chunksize {
					s = chunksize
				}

				// github.com/miekg/dns is a little broken wrt TXT record encoding.
				//
				// Instead of doing escaping/unescaping when formatting TXTs as
				// zonefile records, it requires the escaping to be done internally,
				// and undoes it (!!?) for transmission. Instead, it should hold TXT
				// records in RAM as raw binary data, making packing for transmission
				// trivial, and it should only escape/unescape when working with
				// zonefiles.
				//
				// Given this, we have to escape here. We do it in a dumb way.
				//
				var buf strings.Builder
				buf.Grow(s * 4)
				for _, ch := range bodyBlob[:s] {
					buf.WriteByte('\\')
					buf.WriteByte('0' + ((ch / 100) % 10))
					buf.WriteByte('0' + ((ch / 10) % 10))
					buf.WriteByte('0' + ((ch / 1) % 10))
				}

				txts = append(txts, buf.String())
				bodyBlob = bodyBlob[s:]
			}
			reply.Answer = append(reply.Answer, &dns.TXT{
				Hdr: dns.RR_Header{Name: queriedName, Rrtype: dns.TypeTXT, Class: dns.ClassINET, Ttl: 604800},
				Txt: txts,
			})
		}
		return nil
	case sql.ErrNoRows:
		if len(reply.Answer) == 0 {
			return NXDOMAIN{}
		} else {
			return nil
		}
	default:
		return err
	}
}

func handleRequest(w dns.ResponseWriter, r *dns.Msg) {
	q := strings.ToUpper(r.Question[0].Name)
	qt := r.Question[0].Qtype

	m := &dns.Msg{}
	m.SetReply(r)
	m.Authoritative = true

	_, isUdp := w.RemoteAddr().(*net.UDPAddr)

	if !strings.HasSuffix(q, *root) {
		m.SetRcode(r, dns.RcodeNameError)
	} else {
		if q == *root {
			m.Answer = append(m.Answer, &dns.TXT{
				Hdr: dns.RR_Header{Name: q, Rrtype: dns.TypeTXT, Class: dns.ClassINET, Ttl: 300},
				Txt: []string{
					"server=nscdnd",
					"version=" + version,
					"SPDX-License-Identifier=AGPL-3.0-or-later",
					"source=" + source_url,
				},
			})

		} else {
			name := q[:len(q)-len(*root)-1 /* for the "." */]
			includeTxt := qt == dns.TypeTXT || qt == dns.TypeANY
			err := recordsForName(q, name, m, includeTxt, isUdp)
			switch err {
			case NXDOMAIN{}:
				m.SetRcode(r, dns.RcodeNameError)
			case nil:
				// pass
			default:
				m.SetRcode(r, dns.RcodeServerFailure)
				log.Error(err)
			}
		}
	}

	log.WithFields(log.Fields{
		"udp":         isUdp,
		"truncated":   m.Truncated,
		"rcode":       dns.RcodeToString[m.Rcode],
		"qtype":       dns.Type(qt).String(),
		"question":    q,
		"answerCount": len(m.Answer),
	}).Info("transaction")

	if err := w.WriteMsg(m); err != nil {
		log.Print(err)
	}
}

func serve(net string, port int16) {
	server := &dns.Server{
		Addr:      fmt.Sprintf(":%d", port),
		Net:       net,
		ReusePort: true,
	}
	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func summarizeFiles(db *sql.DB) {
	row := db.QueryRow("select count(*) from files")
	var n int
	err := row.Scan(&n)
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("%d file(s) present at startup time", n)
}

func usage() {
	fmt.Printf("Usage: %s [OPTIONS] <sqlite-filename>\n", os.Args[0])
	fmt.Println()
	flag.PrintDefaults()
	fmt.Println()
	fmt.Printf("Version: %s\n", version)
	fmt.Printf("Source code: %s\n", source_url)
}

func main() {
	flag.Usage = usage
	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		os.Exit(1)
	}

	storeName := flag.Arg(0)

	log.WithFields(log.Fields{
		"server":                  "nscdnd",
		"version":                 version,
		"SPDX-License-Identifier": "AGPL-3.0-or-later",
		"source":                  source_url,
	}).Info("Starting")

	var err error
	db, err = sql.Open("sqlite3", storeName)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Content store %s opened", storeName)

	summarizeFiles(db)

	if !strings.HasSuffix(*root, ".") {
		*root += "."
	}
	*root = strings.ToUpper(*root)

	log.Printf("Serving root domain %s on port %d", *root, *port)

	dns.HandleFunc(*root, handleRequest)

	go serve("tcp", int16(*port))
	go serve("udp", int16(*port))

	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	fmt.Printf("Signal (%s) received, stopping\n", <-sig)
}

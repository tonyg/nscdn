/// SPDX-License-Identifier: AGPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

package nscdn

import (
	"context"
	"fmt"
	"net"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"leastfixedpoint.com/nscdn/pkg/hashtree"
)

type Api struct {
	resolver *net.Resolver
	Domain   string
	Host     string
	Port     int16
}

type BlobSource struct {
	resolver *net.Resolver
	domain   string
}

func (api *Api) Resolver() *net.Resolver {
	if api.resolver == nil {
		api.resolver = net.DefaultResolver
		if len(api.Host) > 0 {
			api.resolver = &net.Resolver{
				PreferGo: true,
				Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
					d := net.Dialer{}
					return d.DialContext(
						ctx,
						network,
						net.JoinHostPort(api.Host, fmt.Sprint(api.Port)))
				},
			}
		}
	}
	return api.resolver
}

func (source *BlobSource) LoadBlob(p hashtree.Pointer) ([]byte, error) {
	var chunks []string
	var err error

	for {
		chunks, err = source.resolver.LookupTXT(context.Background(), p.Label()+"."+source.domain)
		if err == nil {
			break
		}
		if d := err.(*net.DNSError); d != nil && d.Temporary() {
			time.Sleep(time.Millisecond * 250)
			continue
		}
		log.Error(err)
		return nil, fmt.Errorf("Chunk %s missing at domain %s", p.Label(), source.domain)
	}

	if len(chunks) != 1 {
		return nil, fmt.Errorf("Expected exactly one chunk, got %d", len(chunks))
	}
	log.WithFields(log.Fields{
		"label":  p.Label(),
		"domain": source.domain,
		"length": len(chunks[0]),
	}).Debug("retrieved")
	return []byte(chunks[0]), nil
}

func (api *Api) Get(name string) chan hashtree.Chunk {
	var fqdn string
	if !strings.HasSuffix(name, ".") {
		domain := api.Domain
		if domain != "" && !strings.HasSuffix(domain, ".") {
			domain = domain + "."
		}
		fqdn = name + "." + domain
	} else {
		fqdn = name
	}

	var p hashtree.Pointer

	cname, err := api.Resolver().LookupCNAME(context.Background(), fqdn)
	if err == nil {
		fqdn = cname
		log.WithField("fqdn", fqdn).Info("CNAME resolved")
	}

	pieces := strings.SplitN(fqdn, ".", 2)
	p, err = hashtree.FromLabel(pieces[0])
	if err != nil {
		log.Fatal("not found")
	}

	source := &BlobSource{
		resolver: api.Resolver(),
		domain:   pieces[1],
	}

	return hashtree.StreamData(source, p)
}

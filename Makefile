.PHONY: test serve roundtrip format fixcopyright generate clean

test: generate
	go test ./...

serve: generate
	go run ./cmd/nscdn init store.sqlite3
	go run ./cmd/nscdnd -port 8053 store.sqlite3

roundtrip: generate
	go run ./cmd/nscdn add store.sqlite3 SekienAkashita < test/SekienAkashita.jpg
	go run ./cmd/nscdn get -host localhost -port 8053 SekienAkashita.demo.nscdn.org > SekienAkashita.jpg.tmp
	cmp SekienAkashita.jpg.tmp test/SekienAkashita.jpg
	rm -f SekienAkashita.jpg.tmp

format:
	go fmt ./...

fixcopyright:
	-fixcopyright.rkt --file-pattern '**.go' AGPL-3.0-or-later Go '^//+ *' '/// '

generate:
	go generate ./...

clean:
	go clean
	rm -f cmd/nscdnd/metadata.go
